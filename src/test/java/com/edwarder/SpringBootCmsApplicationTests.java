package com.edwarder;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.edwarder.entity.SysRole;
import com.edwarder.entity.SysUser;
import com.edwarder.mapper.SysUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootCmsApplicationTests {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Test
    public void contextLoads() {
       /* String pwd = new BCryptPasswordEncoder().encode("123456");
        SysUser sysUser = new SysUser();
        sysUser.setUsername("admin");
        sysUser.setPassword(pwd);
        sysUserMapper.insert(sysUser);*/
       //sysUserMapper.findList().forEach(System.out::println);
        System.out.println(sysUserMapper.selectById(1));

    }

    @Test
    public void selectListByPage() {
        sysUserMapper.selectList(new QueryWrapper<SysUser>())
                .forEach(System.out::println);
    }

    @Test
    public void insertRoles() {
//        Map<Integer, Integer> map = new HashMap<>();
//        map.put(3,1);
        SysUser sysUser = new SysUser();
        sysUser.setId(4);
        List<SysRole> list = new ArrayList<>();
        SysRole r1 = new SysRole();
        r1.setId(1);
        SysRole r2 = new SysRole();
        r1.setId(2);
        list.add(r1);
        list.add(r2);
        sysUser.setRoles(list);
        Map<Integer, List<SysRole>> map = new HashMap<>();
        map.put(4,list);
//        sysUserMapper.insertRoles(map);
    }

    @Test
    public void deleteRoles() {
        SysUser sysUser = new SysUser();
        sysUser.setId(3);
        sysUserMapper.deleteRoles(sysUser);
    }

}
