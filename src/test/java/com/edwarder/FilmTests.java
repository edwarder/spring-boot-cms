package com.edwarder;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.edwarder.entity.Film;
import com.edwarder.entity.SysMenu;
import com.edwarder.entity.SysRole;
import com.edwarder.mapper.SysRoleMapper;
import com.edwarder.service.FilmService;
import com.edwarder.service.SysRoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FilmTests {
    @Autowired
    private FilmService filmService;

    @Test
    public void contextLoads() {
       filmService.page(new Page<>(),
               new QueryWrapper<Film>().eq("delFlag","1")
       ).getRecords().forEach(System.out::println);
    }

}
