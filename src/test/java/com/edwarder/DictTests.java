package com.edwarder;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.edwarder.entity.SysDict;
import com.edwarder.entity.SysMenu;
import com.edwarder.entity.SysRole;
import com.edwarder.mapper.SysRoleMapper;
import com.edwarder.service.SysDictService;
import com.edwarder.service.SysRoleService;
import com.edwarder.utils.DictUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DictTests {
    @Autowired
    private SysDictService sysDictService;

    @Test
    public void contextLoads() {
        sysDictService.list(new QueryWrapper<SysDict>()
                .eq("typecode","nation")).forEach(System.out::println);
    }

    @Test
    public void testGetValue() {
//        System.out.println(DictUtils.getDictValue("language","1"));
    }


}
