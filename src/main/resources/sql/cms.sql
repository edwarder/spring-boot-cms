/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50722
Source Host           : 10.40.5.1:3306
Source Database       : cms

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-11-13 13:42:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filmid` int(11) DEFAULT NULL,
  `imgurl` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES ('14', '1', '1541121144441.png');
INSERT INTO `album` VALUES ('15', '1', '1541121144451.png');
INSERT INTO `album` VALUES ('16', '1', '1541121144535.png');
INSERT INTO `album` VALUES ('17', '1', '1541121144552.png');
INSERT INTO `album` VALUES ('18', '2', '1541121165702.jpg');
INSERT INTO `album` VALUES ('19', '2', '1541121165718.png');
INSERT INTO `album` VALUES ('20', '2', '1541121165749.jpg');
INSERT INTO `album` VALUES ('21', '2', '1541121165821.jpg');
INSERT INTO `album` VALUES ('22', '2', '1541121165909.jpg');
INSERT INTO `album` VALUES ('23', '3', '1541121189752.jpg');
INSERT INTO `album` VALUES ('24', '3', '1541121189772.jpg');
INSERT INTO `album` VALUES ('25', '3', '1541121189784.jpg');
INSERT INTO `album` VALUES ('26', '3', '1541121189974.jpg');
INSERT INTO `album` VALUES ('27', '4', '1541121208558.jpg');
INSERT INTO `album` VALUES ('28', '4', '1541121208555.jpg');
INSERT INTO `album` VALUES ('29', '4', '1541121208581.jpg');
INSERT INTO `album` VALUES ('30', '4', '1541121208673.jpg');
INSERT INTO `album` VALUES ('31', '5', '1541121221561.jpg');
INSERT INTO `album` VALUES ('32', '5', '1541121221587.jpg');
INSERT INTO `album` VALUES ('33', '5', '1541121221596.jpg');
INSERT INTO `album` VALUES ('34', '5', '1541121221672.jpg');

-- ----------------------------
-- Table structure for film
-- ----------------------------
DROP TABLE IF EXISTS `film`;
CREATE TABLE `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filmname` varchar(50) DEFAULT NULL,
  `filmpic` varchar(100) DEFAULT NULL,
  `actor` varchar(100) DEFAULT NULL,
  `director` varchar(100) DEFAULT NULL,
  `nation` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL COMMENT '类别',
  `language` varchar(100) DEFAULT NULL,
  `summary` text,
  `realeasetime` varchar(255) DEFAULT NULL COMMENT '上映时间',
  `delflag` int(2) DEFAULT '0' COMMENT '逻辑删除标示位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of film
-- ----------------------------
INSERT INTO `film` VALUES ('1', '金刚狼', '1541120458054.png', '休·杰克曼 Hugh Jackman', '詹姆斯·曼高德 James Mangold', '美国', '1', '3', '<p>◎译　　名　金刚狼3：殊死一战/卢根(港)/罗根(台)/金刚狼3：罗根/金刚狼3：暮狼寻乡/这个金刚不太狼(豆友译名)<br>◎片　　名　Logan<br>◎年　　代　2017<br>◎国　　家　<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a><br>◎类　　别　<a href=\"http://www.hao6v.com/s/juqingpian/\" target=\"_blank\" title=\"剧情片\">剧情</a>/<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a>/<a href=\"http://www.hao6v.com/s/kehuan/\" target=\"_blank\" title=\"科幻片\">科幻</a><br>◎语　　言　英语/西班牙语<br>◎字　　幕　中英双字<br>◎上映日期　2017-02-17(柏林电影节)/2017-03-03(<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆/<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a>)<br>◎IMDb评分  8.5/10 from 210,895 users<br>◎豆瓣评分　8.3/10 from 155,060 users<br>◎片　　长　123分钟(<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆)/135分钟(柏林电影节)/137分钟(<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a>)<br>◎导　　演　詹姆斯·曼高德 James Mangold<br>◎主　　演　休·杰克曼 Hugh Jackman<br>　　　　　　帕特里克·斯图尔特 Patrick Stewart<br>　　　　　　达芙妮·金 Dafne Keen<br>　　　　　　波伊德·霍布鲁克 Boyd Holbrook<br>　　　　　　斯戴芬·莫昌特 Stephen Merchant<br>　　　　　　伊丽莎白·罗德里格斯 Elizabeth Rodriguez<br>　　　　　　多丽丝·莫尔加多 Doris Morgado<br>　　　　　　理查德·E·格兰特 Richard E. Grant<br>　　　　　　玛丽·佩顿·斯图尔特 Mary Peyton Stewart<br>　　　　　　伊利斯·尼尔 Elise Neal<br>　　　　　　艾瑞琪·拉·塞拉 Eriq La Salle<br>　　　　　　劳伦·格罗斯 Lauren Gros<br>　　　　　　胡安·贾斯帕 Juan Gaspard<br>　　　　　　克日什托夫·索什斯基 Krzysztof Soszynski<br>　　　　　　萨博·班克森 Saber Bankson</p><p>◎简　　介</p><p>　　故事发生在2029年，彼时，X战警早已经解散，作为为数不多的仅存的变种人，金刚狼罗根（休·杰克曼 Hugh Jackman 饰）和卡利班（斯戴芬·莫昌特 Stephen Merchant 饰）照顾着年迈的X教授（帕特里克·斯图尔特 Patrick Stewart 饰），由于衰老，X教授已经丧失了对于自己超能力的控制，如果不依赖药物，他的超能力就会失控，在全球范围内制造无法挽回的灾难。不仅如此，金刚狼的自愈能力亦随着时间的流逝逐渐减弱，体能和力量都早已经大不如从前。<br>　　某日，一位陌生女子找到了金刚狼，将一个名为劳拉（达芙妮·基恩 Dafne Keen 饰）的女孩托付给他，嘱咐他将劳拉送往位于加拿大边境的“伊甸园”。让罗根没有想到的是，劳拉竟然是被植入了自己的基因而培养出的人造变种人，而在传说中的伊甸园里，有着一群和劳拉境遇相似的孩子。邪恶的唐纳德（波伊德·霍布鲁克 Boyd Holbrook 饰）紧紧的追踪着罗根一行人的踪迹，他的目标只有一个，就是将那群人造变种人彻底毁灭。</p><p><img src=\"http://localhost:9000/cms/article/1541120767317.png\" style=\"max-width:100%;\"><img src=\"http://localhost:9000/cms/article/1541120773978.png\" style=\"max-width: 100%;\"><br></p><p><img src=\"http://localhost:9000/cms/article/1541120780960.png\" style=\"max-width:100%;\"><img src=\"http://localhost:9000/cms/article/1541120785342.png\" style=\"max-width: 100%;\"><br></p>', '2017-02-17(柏林电影节)/2017-03-03(中国大陆/美国)', '0');
INSERT INTO `film` VALUES ('2', '战狼', '1541120479213.jpg', '吴京 Jacky Wu', '吴京 Jacky Wu', '中国大陆', '1', '2', '<p>◎译　　名　新战狼/新战死沙场<br>◎片　　名　战狼2<br>◎年　　代　2017<br>◎产　　地　<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆<br>◎类　　别　<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a><br>◎语　　言　汉语普通话/英语<br>◎字　　幕　中英双字<br>◎上映日期　2017-07-27(<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆)<br>◎IMDb评分  6.7/10 from 2,220 users<br>◎豆瓣评分　7.4/10 from 397,702 users<br>◎片　　长　123分钟<br>◎导　　演　吴京 Jacky Wu<br>◎主　　演　吴京 Jacky Wu<br>　　　　　　弗兰克·格里罗 Frank Grillo<br>　　　　　　吴刚 Gang Wu<br>　　　　　　张翰 Han Zhang<br>　　　　　　卢靖姗 Celina Jade<br>　　　　　　丁海峰 Haifeng Ding<br>　　　　　　淳于珊珊 Shanshan Chunyu<br>　　　　　　余男 Nan Yu<br>　　　　　　于谦 Qian Yu<br>　　　　　　石兆琪 Zhaoqi Shi<br>　　　　　　海蒂·玛尼梅可 Heidi Moneymaker<br>　　　　　　奥列格·普鲁迪乌斯 Oleg Aleksandrovich Prudius<br>　　　　　　阿隆·汤尼 Aaron Toney<br>　　　　　　泰勒·哈里斯 Thayr Harris<br>　　　　　　勃小龙 Pierre Bourdaud</p><p>◎简　　介</p><p>　　故事发生在非洲附近的大海上，主人公冷锋（吴京 饰）遭遇人生滑铁卢，被“开除军籍”，本想漂泊一生的他，正当他打算这么做的时候，一场突如其来的意外打破了他的计划，突然被卷入了一场非洲国家叛乱，本可以安全撤离，却因无法忘记曾经为军人的使命，孤身犯险冲回沦陷区，带领身陷屠杀中的同胞和难民，展开生死逃亡。随着斗争的持续，体内的狼性逐渐复苏，最终孤身闯入战乱区域，为同胞而战斗。</p><p><img src=\"http://localhost:9000/cms/article/1541120533276.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"http://localhost:9000/cms/article/1541120546051.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"http://localhost:9000/cms/article/1541120552250.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"http://localhost:9000/cms/article/1541120559867.jpg\" style=\"max-width:100%;\"><br></p>', '2017-07-27(中国大陆)', '0');
INSERT INTO `film` VALUES ('3', '星际迷航3', '1541120829357.jpg', '克里斯·派恩 Chris Pine', '林诣彬 Justin Lin', '美国', '1', '3', '<p>◎译　　名　星际迷航3：超越星辰/星际迷航13：超越/星际旅行13：超越太空/星空奇遇记13：超域时空(港)/星际争霸战13：浩瀚无垠(台)/星舰奇航记13/星舰迷航记13<br>◎片　　名　Star Trek Beyond<br>◎年　　代　2016<br>◎国　　家　<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a><br>◎类　　别　<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a>/<a href=\"http://www.hao6v.com/s/kehuan/\" target=\"_blank\" title=\"科幻片\">科幻</a>/<a href=\"http://www.hao6v.com/s/maoxian/\" target=\"_blank\" title=\"冒险片\">冒险</a><br>◎语　　言　英语<br>◎字　　幕　中英双字<br>◎上映日期　2016-07-22(<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a>)/2016-09-02(<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆)<br>◎IMDb评分  7.4/10 from 74,918 users<br>◎豆瓣评分　7.5/10 from 57,361 users<br>◎片　　长　122分钟(<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆/<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a>)<br>◎导　　演　林诣彬 Justin Lin<br>◎主　　演　克里斯·派恩 Chris Pine<br>　　　　　　扎克瑞·昆图 Zachary Quinto<br>　　　　　　佐伊·索尔达娜 Zoe Saldana<br>　　　　　　西蒙·佩吉 Simon Pegg<br>　　　　　　安东·叶利钦 Anton Yelchin<br>　　　　　　伊德里斯·艾尔巴 Idris Elba<br>　　　　　　索菲亚·波多拉 Sofia Boutella<br>　　　　　　卡尔·厄本 Karl Urban<br>　　　　　　索瑞·安达斯鲁 Shohreh Aghdashloo<br>　　　　　　约翰·赵 John Cho<br>　　　　　　迪普·罗伊 Deep Roy<br>　　　　　　莉迪亚·威尔逊 Lydia Wilson<br>　　　　　　乔·塔斯利姆 Joe Taslim<br>　　　　　　约瑟夫·盖特 Joseph Gatt<br>　　　　　　卡洛·安切洛蒂 Carlo Ancelotti<br>　　　　　　亚当·迪马克 Adam DiMarco<br>　　　　　　梅丽莎·罗斯伯格 Melissa Roxburgh<br>　　　　　　阿什丽·艾德纳 Ashley Edner<br>　　　　　　贾森·马修·史密斯 Jason Matthew Smith</p><p>◎简　　介</p><p>　　柯克船长（克里斯·派恩 Chris Pine 饰）和企业号的船员们来到了银河系中未知的一个区域，开始完成他们5年的任务——探索新世界，寻找新物种。却在途中滞留异星，遭遇了当地种族追杀，他们必须找到方法离开这个星球。</p><p><br></p><p><img src=\"http://localhost:9000/cms/article/1541120878623.jpg\" style=\"max-width:100%;\">  <img src=\"http://localhost:9000/cms/article/1541120883608.jpg\" style=\"max-width: 100%;\"><br></p><p><img src=\"http://localhost:9000/cms/article/1541120894095.jpg\" style=\"max-width:100%;\">  <img src=\"http://localhost:9000/cms/article/1541120899167.jpg\" style=\"max-width: 100%;\"><br></p>', '2016-07-22(美国)/2016-09-02(中国大陆)', '0');
INSERT INTO `film` VALUES ('4', '西虹市首富', '1541120952067.jpg', '沈腾 Teng Shen', '闫非 Fei Yan / 彭大魔 Damo Peng', '中国大陆', '2', '1', '<p>◎译　　名　资本接班人<br></p><p>◎片　　名　西虹市首富<br>◎年　　代　2018<br>◎产　　地　<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆<br>◎类　　别　<a href=\"http://www.hao6v.com/s/xiju/\" target=\"_blank\" title=\"喜剧片\">喜剧</a><br>◎语　　言　汉语普通话<br>◎字　　幕　中字<br>◎上映日期　2018-07-27(<a href=\"http://www.hao6v.com/s/guochandianying/\" target=\"_blank\" title=\"国产电影\">中国</a>大陆)<br>◎IMDb评分&nbsp; 6.2/10 from 415 users<br>◎豆瓣评分　6.7/10 from 347395 users<br>◎片　　长　118分钟<br>◎导　　演　闫非 Fei Yan / 彭大魔 Damo Peng<br>◎主　　演　沈腾 Teng Shen<br>　　　　　　宋芸桦 Vivian Sung<br>　　　　　　张一鸣 Yiming Zhang<br>　　　　　　张晨光 Morning Chang<br>　　　　　　常远 Yuan Chang<br>　　　　　　魏翔 Xiang Wei<br>　　　　　　赵自强 Tzu-chiang Chao<br>　　　　　　九孔 Kong Jiu<br>　　　　　　李立群 Lichun Lee<br>　　　　　　王成思 Chengsi Wang<br>　　　　　　徐冬冬 Dongdong Xu<br>　　　　　　艾伦 Allen<br>　　　　　　杨皓宇 Haoyu Yang<br>　　　　　　黄才伦 Cailun Huang<br>　　　　　　包贝尔 Bei\'er Bao<br>　　　　　　王力宏 Lee-Hom Wang<br>　　　　　　张绍刚 Shaogang Zhang<br>　　　　　　郎咸平 Larry Hsien Ping Lang<br>　　　　　　杨文哲 Wenzhe Yang<br>　　　　　　陶亮 Liang Tao<br>　　　　　　王赞 Zan Wang<br>　　　　　　黄杨 Yang Huang<br>　　　　　　刘鉴 Jian Liu<br>　　　　　　杨沅翰 Yuanhan Yang<br>　　　　　　林炳宝 Bingbao Lin<br>　　　　　　骆佳 Jia Luo<br>　　　　　　陈昊明 Haoming Chen<br>　　　　　　臧一人 Yiren Zang</p><p>◎简　　介<br>　　《西虹市首富》的故事发生在《夏洛特烦恼》中的“特烦恼”之城“西虹市”。混迹于丙级业余足球队的守门员王多鱼（沈腾饰演），因比赛失利被开除离队。正处于人生最低谷的他接受了<a href=\"http://www.hao6v.com/s/shenmi/\" target=\"_blank\" title=\"神秘片\">神秘</a>台湾财团“一个月花光十亿资金”的挑战。本以为快乐生活就此开始，王多鱼却第一次感到“花钱特烦恼”！想要人生反转走上巅峰，真的没有那么简单。</p><p><br></p><p><img src=\"http://localhost:9000/cms/article/1541120974373.jpg\" style=\"max-width:100%;\">&nbsp;&nbsp;<img src=\"http://localhost:9000/cms/article/1541120981456.jpg\" style=\"max-width: 100%;\"><br></p><p><img src=\"http://localhost:9000/cms/article/1541120988035.jpg\" style=\"max-width:100%;\">&nbsp;&nbsp;<img src=\"http://localhost:9000/cms/article/1541120993767.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>', '2018-07-27(中国大陆)', '0');
INSERT INTO `film` VALUES ('5', '阿凡达', '1541121006397.jpg', '萨姆·沃辛顿 Sam Worthington....Jake Sully', '詹姆斯·卡梅隆 James Cameron', '美国/英国', '1', '3', '<p>◎译　　名　阿凡达/化身/异次元战神/天神下凡/神之化身<br>◎片　　名　Avatar<br>◎年　　代　2009<br>◎国　　家　<a href=\"http://www.hao6v.com/s/hanguodianying/\" target=\"_blank\" title=\"美国电影\">美国</a>/英国<br>◎类　　别　<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a>/<a href=\"http://www.hao6v.com/s/maoxian/\" target=\"_blank\" title=\"冒险片\">冒险</a>/<a href=\"http://www.hao6v.com/s/huanxiang/\" target=\"_blank\" title=\"幻想片\">幻想</a>/<a href=\"http://www.hao6v.com/s/kehuan/\" target=\"_blank\" title=\"科幻片\">科幻</a><br>◎语　　言　英语/西班牙语<br>◎字　　幕　中英双字<br>◎字幕语言　中文/英文<br>◎视频尺寸　1280×720<br>◎文件格式　RMVB+AAC<br>◎IMDB评分　8.3/10 (270,847 votes) Top 250: #125<br>◎文件格式　X264 + AC3<br>◎视频尺寸　1280 x 720<br>◎片　　长　2:58:09<br>◎导　　演　詹姆斯·卡梅隆 James Cameron<br>◎主　　演　萨姆·沃辛顿 Sam Worthington....Jake Sully<br>　　　　　　佐伊·索尔达娜 Zoe Saldana....Neytiri<br>　　　　　　西格妮·韦弗 Sigourney Weaver....Grace<br>　　　　　　史蒂芬·朗 Stephen Lang....Colonel Miles Quaritch<br>　　　　　　米歇尔·罗德里格兹 Michelle Rodriguez....Trudy Chacon<br>　　　　　　吉奥瓦尼·瑞比西 Giovanni Ribisi....Parker Selfridge<br>　　　　　　乔·摩尔 Joel Moore....Norm Spellman (as Joel David Moore)<br>　　　　　　希·庞德 CCH Pounder....Moat<br>　　　　　　韦斯·斯塔迪 Wes Studi....Eytukan<br>　　　　　　拉兹·阿隆索 Laz Alonso....Tsu\'tey<br>　　　　　　迪利普·劳 Dileep Rao....Dr. Max Patel<br>　　　　　　Matt Gerald ....Corporal Lyle Wainfleet<br>　　　　　　Sean Anthony Moran ....Private Fike<br>　　　　　　Jason Whyte ....Cryo Vault Med Tech<br>　　　　　　Scott Lawrence ....Venture Star Crew Chief<br>　　　　　　Kelly Kilgour ....Lock Up Trooper<br>　　　　　　J. Patrick Pitts ....Shuttle Pilot<br>　　　　　　Sean Patrick Murphy ....Shuttle Co-Pilot<br>　　　　　　Peter Dillon ....Shuttle Crew Chief<br>　　　　　　Kevin Dorman ....Tractor Operator<br>　　　　　　Kelson Henderson ....Dragon Gunship Pilot<br>　　　　　　David Van Horn ....Dragon Gunship Gunner<br>　　　　　　Jacob Tomuri ....Dragon Gunship Navigator<br>　　　　　　Michael Blain-Rozgay ....Suit #1<br>　　　　　　Jon Brent Curry ....Suit #2<br>　　　　　　Julene Renee ....Ambient Room Tech<br>　　　　　　Luke Hawker ....Ambient Room Tech<br>　　　　　　Woody Schultz ....Ambient Room Tech<br>　　　　　　Peter Mensah ....Horse Clan Leader<br>　　　　　　Sonia Yee ....Link Room Tech<br>　　　　　　Jahnel Curfman ....Basketball Avatar<br>　　　　　　Ilram Choi ....Basketball Avatar<br>　　　　　　Kyla Warren ....Na\'vi Child<br><br>◎简　　介<br><a href=\"http://www.hao6v.com/s/juqingpian/\" target=\"_blank\" title=\"剧情片\">剧情</a><br>　　当贪婪的人类将地球的资源和生存环境开采得所剩无几的时候，他们把目光投向了宇宙中的潘多拉星球。在大批生物的进驻下，潘多拉星球已经不再<a href=\"http://www.hao6v.com/s/shenmi/\" target=\"_blank\" title=\"神秘片\">神秘</a>。那里有各种各样的植物和生物，有身高近三米、生活在原始丛林中的土著——纳威人，还有人类渴望得到的资源。<br>　　杰克是一名失去行走能力的前海军陆战队队员，他来到潘多拉星球纯属一个偶然。杰克的孪生哥哥一直在从事人类进驻潘多拉星球的计划，并且拥有一个用他的DNA制造成的“阿凡达”，那是一种人类和纳威人DNA结合而成的物种，外貌和纳威人一样，拥有者用精神力操控的阿凡达。可是，在前往潘多拉星球前，杰克的哥哥去世了，于是，作为和哥哥DNA最接近的杰克，成为了这项使命的继任者。<br>　　在操控阿凡达的过程中，杰克再次感受到了用脚走路的感觉，接触到了从未见过的世界，甚至比其他阿凡达同伴更快接触到了纳威人的世界。他被纳威人允许跟随族长的女儿Neytiri 学习纳威男人应该会的一切。在学习中，杰克逐渐改变了最初被灌输的想法，他感受到，那也是一个世界，和人类世界别无二致，人类不应该因为自己的贪婪而把对方简化成“未开化的、野蛮的物种”进而堂而皇之地剿灭对方以霸占对方的物产。除此之外，杰克也获得了自己的<a href=\"http://www.hao6v.com/s/aiqing/\" target=\"_blank\" title=\"爱情片\">爱情</a>，和Neytiri 坠入爱河。<br>　　当纳威人的灭顶之灾来临的时候，杰克作为一个有良知的阿凡达和人类伙伴们率领纳威人与“地球军团”进行抗争，惨烈但是充满意义的战斗之后，杰克终于作为一个真正的纳威人留在了潘多拉星球。<br>一句话评论<br>卡梅隆这部历时十余年打造的史诗巨作呈现了独一无二的宏伟场面，壮观的视野，激动人心的叙事以及回归自然的主题。<br>——《综艺》<br>卡梅隆运用了他掌握的所有视觉工具，像一位战略大师一样带领着观众，为众人展示战斗中的每一个转折，展现每一个改变故事进程的勇士们的死亡与怯弱者的表情。<br>——《好莱坞报道》<br>没有任何电影事件能够超过《阿凡达》，它无论从哪个角度来看都是气势恢宏的：不管是野心或是视觉效果，又或是开创性的制作技术，甚至于令人咋舌的制片成本。<br>——《泰晤士报》<br>幕后制作<br>　　十二年磨一剑<br>　　自1997年《泰坦尼克号》在全球大热之后，导演詹姆斯·卡梅隆的下一部电影令影迷等得几乎望眼欲穿。对此，詹姆斯·卡梅隆表示：\"《阿凡达》能够最终拍出来，是一次奇迹。这是一部<a href=\"http://www.hao6v.com/s/kehuan/\" target=\"_blank\" title=\"科幻片\">科幻</a>电影，是我喜欢的东西，<a href=\"http://www.hao6v.com/s/kehuan/\" target=\"_blank\" title=\"科幻片\">科幻</a>电影要表现的是我们现在无法接触到的事物，这是一种预言，它会让你反思我们现在在做的一切，将来会发生什么样的后果？这就是我拍《阿凡达》的初衷之一，我对现在人类对大自然所做的一切感到深深的忧虑，我想将来也许会受到大自然的报复。\"<br>　　《泰坦尼克号》其北美6亿，全球18亿的票房成绩至今没有电影能超越，谈及这所带来的压力，詹姆斯·卡梅隆说：\"《泰坦尼克号》的成功的确是一件让人满足的事，我在之后拍摄了数部<a href=\"http://www.hao6v.com/s/jilu/\" target=\"_blank\" title=\"纪录片\">纪录</a>片，可以说已经基本脱离了原来的圈子。我需要改变一种工作和生活的方式，拍摄那样的电影是一件非常折磨人的事。离开了近10年，我又回来了，人们会有期待，也会怀疑，我究竟还能不能拍电影？大家这样想再正常不过，《泰坦尼克号》的票房？的确是太好了，我不认为《阿凡达》能打破《泰坦尼克号》的票房记录，我拍这部电影的目的也不是为了打破什么，更何况我也不认为未来数年内能有什么电影超过这个记录。只要《阿凡达》能够让投资方赚钱，我就感到很高兴了。\"<br>　　为影片开出巨额投资的福克斯老上级新闻集团，其首席执行官、传媒大王鲁伯特·默多克，也罕见地针对一部影片发表了较多的言论，在谈及影片的制作与投资时，鲁伯特·默多克说：\"对这部电影我很有信心，詹姆斯·卡梅隆的艺术魅力是举世公认的，我想没有人会怀疑他对于电影的商业价值，因此我们为《阿凡达》所做的一切都是在计划中的。这部电影所牵涉的方方面面都很复杂，投资与票房仅仅只是其一部分，我希望观众能够尽可能的关注影片本身，它是一部你应该走到电影院坐下来静下心欣赏的电影，我们把它安排在了圣诞档期，连我自己都对《阿凡达》的上映迫不及待了。\"<br>　　电脑特效的里程碑?<br>　　詹姆斯·卡梅隆对电脑特效的迷恋与狂热众人皆知，《终结者》、《泰坦尼克号》等作品都采用了不少的CGI，而在《阿凡达》里，CGI的制作更达到了一个崭新的境界。本片的电脑特效制作部门主管文斯·佩斯说：\"这部电影所使用的CGI技术是全新的，所有的设备都是为这部电影而打造，我们希望《阿凡达》能成为CGI技术的一次革命，能成为此后电影制作的一种选择。观众们会在这部电影里看到过去难以想像的许多场面，特别是3D版本。\"<br>　　詹姆斯·卡梅隆对《阿凡达》里制作CGI的过程谈到：\"这其实是拍摄本片最难的一段时间。我不希望重复自己，在上个世纪我想要拍摄一部新CGI技术的电影，那个时候困难重重，大家都对我说办不到，我等了这么多年，终于能够有机会做一部真正全新CGI技术的电影了。全新意味着一切都要靠自己摸索，没有任何东西可以借鉴，大家都并不清楚我们能做到什么地步，在一开始的确很迷茫……幸好这段痛苦的时间也很短暂，当CGI开始上了轨道，我们都明白这部电影最终呈现的效果会是什么样了。我们为这部电影设计了全新的表情捕捉系统，它可以捕捉到演员的每一个细微表情。而这个系统会让詹姆斯｀卡梅隆立即从监视器中看到真实演员和虚拟环境的互动，这让他可以更好地执导演员和电脑生成的角色和环境配戏。\"<br>　　《阿凡达》的演员们大部分时间都在绿幕前进行拍摄，而担任影片男一号的萨姆·沃辛顿对于这种拍摄方法说道：\"我以前也拍过类似的电影，但跟《阿凡达》比起来差别实在太大了。整个电影让我印象最深刻的是有一段我扮演的角色穿越丛林的戏，詹姆斯·卡梅隆事先给我讲述了整个场景的氛围，能见度多少，气温多少，树木有多高，草丛有多深，我一共要走多少路，我行走的路线是如何，路上遇见什么样的生物，我需要做什么样的<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a>……这一切都需要我想象出来，然后在摄影棚里做出相应的<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a>。天，这样的感觉实在太奇怪了，而当我最后发现做出来的<a href=\"http://www.hao6v.com/s/dongzuo/\" target=\"_blank\" title=\"动作片\">动作</a>居然能够和虚拟的电脑制作的画面契合在一起的时候，尽管我已经经历过不少次这种拍摄经历了，但我必须要说的是，《阿凡达》是我做过最不可思议的一次！\"<br>　　全新的3D之旅<br>　　《阿凡达》采用了时下热门的3D拍摄方式，谈及3D影院对《阿凡达》的影响，詹姆斯·卡梅隆说：\"3D技术并不是简单唬唬小孩子的玩意，它能让观众在观看电影的时候更加投入，犹如身临其境一般。我不希望观众是在看这个电影，看《阿凡达》的世界，而是通过3D技术，能够进入这个世界。它会将在家里看碟和电影院看的效果形成天壤之别，我希望3D技术能够吸引大家进电影院观看《阿凡达》。\"<br>　　最后詹姆斯·卡梅隆对本片说出了他的感言：\"这部电影的前前后后筹备、制作了许多年，有数百位电影制作人员奉献出了他们的心血，终于等到上映的这一天了，我和大家一样感到兴奋……我的野心是让人们重新点燃数十年前首次观看《2001太空漫游》或《星球大战》时的兴奋与激情！届时这股旋风会将你吹至影院屏幕的后墙上……我对《阿凡达》的期望已经远远超过了过去拍摄的那些电影，有人称《阿凡达》会给观众带来一场视觉革命，我要做的并不仅限于此，我希望大家能够通过《阿凡达》，感受到过去的电影从未带给你的那种震撼！\"<br>花絮<br>·《阿凡达》是詹姆斯·卡梅隆继1997年《泰坦尼克号》之后的第一部故事片。<br>·《阿凡达》采用了普通影院版本、3D影院版本、IMAX－3D三种上映方式。<br>·《阿凡达》的北美公映日期是12月18日，十二年前，詹姆斯·卡梅隆的作品《泰坦尼克号》的公映日期同样也是12月18日。<br>·根据《纽约时代》的消息，数年间，本片前前后后众多的投资者已经在影片的成本、宣传等各方面花费了超过5亿美元，其中福克斯的投资为3亿美元，派拉蒙等其他投资商的投资成本为2亿美元。这使得《阿凡达》成为电影史上投资最高的作品。<br>·为确保高额的投资能够收回，本片的续集计划已经在筹备中，如果《阿凡达》的票房能够取得成功，它很可能会发展成一个系列电影。<br>·《阿凡达》在筹备期间的片名为《880计划》\"Project 880\"，最后确定的片名为《阿凡达》（Avatar），片名源自<a href=\"http://www.hao6v.com/s/yindudianying/\" target=\"_blank\" title=\"印度电影\">印度</a>佛教用语，意指灵魂被转移至新的躯体里面。<br>·为打造本片的<a href=\"http://www.hao6v.com/s/kehuan/\" target=\"_blank\" title=\"科幻片\">科幻</a>场景和新的视觉效果，詹姆斯·卡梅隆和好莱坞电影技术大师文斯·佩斯特别开发研制了新的摄影系统与数字技术，造价约为1400万美元。<br>·本片的宣传口号是：\"一个全新的世界等着你\"（An All New World Awaits）。　<br>·本片的计划从1998年就开始，当时詹姆斯·卡梅隆希望《阿凡达》能够趁着《泰坦尼克号》的全球风靡，使得本片能够在1999年上映，但由于开出高额的4亿投资使得没有一间电影公司能够接此订单，结果导致本片的拍摄计划一直遥遥无期。<br>·影片有60％的场景是全电脑特效制作。<br>精彩对白<br>Col. Quaritch: You are not in Kansas anymore. You are on Pandora, ladies and gentleman. <br>Selfridge: This is why we\'re here, because this little gray rock sells for twenty million a kilo. <br>夸里奇上校：\"你们再也不会呆在堪萨斯州了。女士们先生们，现在你们身处的是潘多拉星球。\"<br>塞尔弗里奇：\"这就是为什么为什么来这里的原因：因为这里有着2000万公斤的的矿石。\"<br>Jake Sully: They\'ve sent us a message... that they can take whatever they want. Well we will send them a message. That this... this is our land! <br>Neytiri: You should not be here.<br>杰克·萨利：\"他们已经给我们发出了一个信息……他们可以得到他们想要的任何东西。我们也会发出一个信息。这是……这是我们的土地！\"<br>内泰丽：\"你不应该在这里。\"<br>Dr. Grace Augustine: Just relax and let your mind go blank. That shouldn\'t be too hard for you. <br>格雷斯·奥古斯丁博士：\"尽管放松，让你的大脑一片空白。这应该不会太难为你。\" </p><p><br></p><p><img src=\"http://localhost:9000/cms/article/1541121049191.jpg\" style=\"max-width: 100%;\">  <img src=\"http://localhost:9000/cms/article/1541121054475.jpg\" style=\"max-width: 100%;\"></p><p><img src=\"http://localhost:9000/cms/article/1541121059395.jpg\" style=\"max-width: 100%;\">  <img src=\"http://localhost:9000/cms/article/1541121064056.jpg\" style=\"max-width: 100%;\"> </p>', '2009', '1');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typecode` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '类型',
  `name` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '字典名',
  `value` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '字典值',
  `description` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', 'gender', '1', '男', '性别');
INSERT INTO `sys_dict` VALUES ('2', 'gender', '2', '女', '性别');
INSERT INTO `sys_dict` VALUES ('3', 'orderStatus', '1', '未付款', '订单状态');
INSERT INTO `sys_dict` VALUES ('4', 'orderStatus', '2', '待付款', '订单状态');
INSERT INTO `sys_dict` VALUES ('5', 'orderStatus', '3', '已付款', '订单状态');
INSERT INTO `sys_dict` VALUES ('6', 'orderStatus', '4', '已收货', '订单状态');
INSERT INTO `sys_dict` VALUES ('7', 'filmtype', '1', '动作', '类型');
INSERT INTO `sys_dict` VALUES ('8', 'filmtype', '2', '剧情', '类型');
INSERT INTO `sys_dict` VALUES ('9', 'filmtype', '3', '情感', '类型');
INSERT INTO `sys_dict` VALUES ('10', 'filmtype', '4', '记录', '类型');
INSERT INTO `sys_dict` VALUES ('11', 'language', '1', '汉语普通话', '语言');
INSERT INTO `sys_dict` VALUES ('12', 'language', '2', '汉语粤语', '语言');
INSERT INTO `sys_dict` VALUES ('13', 'language', '3', '英语', '语言');
INSERT INTO `sys_dict` VALUES ('14', 'language', '4', '法语', '语言');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `createuser` int(11) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `updateuser` int(11) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('2', '1', '控制台', '/a/console', null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('3', '1', '用户管理', '/a/user/list', null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', '/a/menu/list', null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('5', '1', '角色管理', '/a/role/list', null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('6', '0', '影片管理', null, null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('7', '6', '图集', '/a/album/list', null, null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('14', '6', '影片列表', '/a/film/list', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL,
  `createuser` int(11) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `updateuser` int(11) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'admin', null, null, null, null);
INSERT INTO `sys_role` VALUES ('2', '分店管理员', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `sys_menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('88', '4', '1');
INSERT INTO `sys_role_menu` VALUES ('89', '4', '2');
INSERT INTO `sys_role_menu` VALUES ('94', '2', '7');
INSERT INTO `sys_role_menu` VALUES ('95', '2', '6');
INSERT INTO `sys_role_menu` VALUES ('96', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('97', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('98', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('99', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('100', '1', '4');
INSERT INTO `sys_role_menu` VALUES ('101', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('102', '1', '5');
INSERT INTO `sys_role_menu` VALUES ('103', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('104', '1', '7');
INSERT INTO `sys_role_menu` VALUES ('105', '1', '6');
INSERT INTO `sys_role_menu` VALUES ('106', '1', '14');
INSERT INTO `sys_role_menu` VALUES ('107', '1', '6');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `status` tinyint(10) DEFAULT NULL,
  `createuser` int(11) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `updateuser` int(11) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', null, '$2a$10$5vIvyT6fpLvhEzDV8geoJuDIYEvlImG38ODkDfk7B8gmGG6Y7jS0S', '1104975916@qq.com', '15250420158', '0', null, null, null, null);
INSERT INTO `sys_user` VALUES ('2', 'tony', 'tony', '$2a$10$Obxwu29fB.FXvlNB9tXMHOQZzwZh4MkqsMbGktwJzYCNCZYIMD1ra', '1918082411@qq.com', '15250420158', '0', null, null, null, null);
INSERT INTO `sys_user` VALUES ('3', 'edwarder', '1', '$2a$10$eIZhfif.qxB9Rc2EU7/98e/y9hFjO1SQNyC/OzH0U8jJCWUkzOiFG', '1', '1', null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('4', '2', '2', '$2a$10$1C9.J29XdYL6.Jxvtv31fe4dBIC2tXihlq7s5W97RwPvbv4/DBq8.', '2', '2', null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('5', '4', '4', '$2a$10$mpLTCSOOa7cy6rRchjzStuUe7u3zxFZvW9F74XussDJfcEXLEXsgq', '4', '4d', null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('6', '1', '1', '$2a$10$w0.Vux6l1tm6pmZjAqzJ/euiF3CNQlCkCqQz1z4YQOFjkf1rRz5Ra', '1', '1', null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sys_user_id` int(20) DEFAULT NULL COMMENT '用户ID',
  `sys_role_id` int(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1');
INSERT INTO `sys_user_role` VALUES ('8', '4', '2');
INSERT INTO `sys_user_role` VALUES ('10', '5', '2');
INSERT INTO `sys_user_role` VALUES ('11', '2', '2');
INSERT INTO `sys_user_role` VALUES ('12', '6', null);
INSERT INTO `sys_user_role` VALUES ('13', '6', '2');
