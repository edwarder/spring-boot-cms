package com.edwarder.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.List;

public class Film extends Model<Film> {
	@TableId(type = IdType.AUTO)
	private Integer id;
	private String filmname;
	private String filmpic;
	private String summary;
	private String actor;
	private String director;
	private String nation;
	private String type;
	private String language;
	private String realeasetime;

	@TableField(value = "delflag")
	@TableLogic
	private Integer delFlag;
	@TableField(exist = false)
	private List<Album> albums;
	public Film() {
		super();
	}

	public Film(String filmname, String filmpic, String summary) {
		this.filmname = filmname;
		this.filmpic = filmpic;
		this.summary = summary;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilmname() {
		return filmname;
	}

	public void setFilmname(String filmname) {
		this.filmname = filmname;
	}

	public String getFilmpic() {
		return filmpic;
	}

	public void setFilmpic(String filmpic) {
		this.filmpic = filmpic;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public List<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRealeasetime() {
		return realeasetime;
	}

	public void setRealeasetime(String realeasetime) {
		this.realeasetime = realeasetime;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	@Override
	public String toString() {
		return "Film{" +
				"id=" + id +
				", filmname='" + filmname + '\'' +
				", filmpic='" + filmpic + '\'' +
				", summary='" + summary + '\'' +
				", actor='" + actor + '\'' +
				", director='" + director + '\'' +
				", nation='" + nation + '\'' +
				", type='" + type + '\'' +
				", language='" + language + '\'' +
				", realeasetime='" + realeasetime + '\'' +
				", delFlag=" + delFlag +
				", albums=" + albums +
				'}';
	}
}
