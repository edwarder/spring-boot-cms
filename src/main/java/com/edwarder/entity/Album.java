package com.edwarder.entity;

import lombok.Data;

/**
 * @Auther: jzhang
 * @Date: 2018/10/26 16:09
 * @Description:    相册
 */
public class Album {
    private Integer id;
    private Integer filmid;
    private String imgurl;

    public Album() {
    }

    public Album(Integer filmid, String imgurl) {
        this.filmid = filmid;
        this.imgurl = imgurl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFilmid() {
        return filmid;
    }

    public void setFilmid(Integer filmid) {
        this.filmid = filmid;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", filmid=" + filmid +
                ", imgurl='" + imgurl + '\'' +
                '}';
    }
}
