package com.edwarder.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edwarder.entity.Album;
import com.edwarder.entity.Film;
import com.edwarder.mapper.FilmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: jzhang
 * @Date: 2018/10/23 22:33
 * @Description:
 */
@Service
public class FilmService extends ServiceImpl<FilmMapper,Film>{
    public static Integer CURRENT_PAGE = 1;
    public static Integer PAGE_SIZE = 2;

    @Autowired
    private FilmMapper filmMapper;

    public IPage<Film> findPageList(Integer page, Integer size) {
        if (page != null) {
            CURRENT_PAGE = page;
        }
        if (size != null) {
            PAGE_SIZE = size;
        }
        IPage<Film> pageFilm = filmMapper.selectPage(
                new Page<>(CURRENT_PAGE, PAGE_SIZE),
                new QueryWrapper<>());
        return pageFilm;
    }

    public void saveFilm(Film film) {
        if(film.getId() != null) {
            filmMapper.updateById(film);
        } else {
            filmMapper.insert(film);
        }
    }

    public void saveAlbum(Integer filmId, List<String> list) {
        Film film = new Film();
        film.setId(filmId);
        List<Album> albums = new ArrayList<>();

        for (String imgurl : list) {
            Album album = new Album();
            album.setImgurl(imgurl);
            albums.add(album);
        }
        film.setAlbums(albums);
        filmMapper.insertAlbumByFilm(film);
    }
}
