package com.edwarder.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edwarder.entity.SysRole;
import com.edwarder.entity.SysUser;
import com.edwarder.mapper.SysUserMapper;
import com.edwarder.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: jzhang
 * @Date: 2018/10/16 09:40
 * @Description:
 */
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> {
    public static Integer CURRENT_PAGE = 1;
    public static Integer PAGE_SIZE = 2;

    @Autowired
    private SysUserMapper sysUserMapper;

    public IPage<SysUser> findList(Integer page, Integer size) {
        if (page != null) {
            CURRENT_PAGE = page;
        }
        if (size != null) {
            PAGE_SIZE = size;
        }
        return sysUserMapper.selectPage(
                new Page<>(CURRENT_PAGE,PAGE_SIZE),
                new QueryWrapper<>());
    }

    public SysUser findUserById(Integer id) {
        return sysUserMapper.findUserById(id);
    }

    /**
     * 根据用户名查询用户
     * @param
     */
    public SysUser findUserByName(String username) {
        return sysUserMapper.findUserByName(username);
    }

    public void saveSysUser(SysUser sysUser) {
        if (sysUser.getId() != null) {
            sysUserMapper.updateById(sysUser);
        } else {
            //新增用户设置默认密码123456
            String pwd = new BCryptPasswordEncoder().encode("123456");
            sysUser.setPassword(pwd);
            Integer index = sysUserMapper.insert(sysUser);
        }
        //更新角色
        updataSysUserRoles(sysUser);
    }

    /**
     * 更新用户角色
     */
    private void updataSysUserRoles(SysUser sysUser) {
        //删除原有角色
        sysUserMapper.deleteRoles(sysUser);
        //更新角色
        sysUserMapper.insertRoles(sysUser);
    }

    /**
     * 更新密码
     */
    public void updataPwd(SysUser sysUser){
        SysUser user = new SysUser();
        user.setId(sysUser.getId());
        String pwd = new BCryptPasswordEncoder().encode(sysUser.getPassword());
        user.setPassword(pwd);
        sysUserMapper.updateById(user);
    }

}
