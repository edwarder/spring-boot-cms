package com.edwarder.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edwarder.entity.Album;
import com.edwarder.mapper.album.AlbumMapper;
import org.springframework.stereotype.Service;

/**
 * @Auther: jzhang
 * @Date: 2018/10/26 16:12
 * @Description:
 */
@Service
public class AlbumService extends ServiceImpl<AlbumMapper, Album> {
}
