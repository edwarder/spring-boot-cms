package com.edwarder.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edwarder.entity.SysDict;
import com.edwarder.mapper.SysDictMapper;
import org.springframework.stereotype.Service;

/**
 * @Auther: jzhang
 * @Date: 2018/11/2 16:49
 * @Description:
 */
@Service
public class SysDictService extends ServiceImpl<SysDictMapper, SysDict> {
}
