package com.edwarder.config;

import com.edwarder.dialect.CustomDialect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: jzhang
 * @Date: 2018/11/5 11:04
 * @Description: 注册方言
 */
@Configuration
public class DialectConfig {
    @Bean
    @ConditionalOnMissingBean
    public CustomDialect customDialect() {
        return new CustomDialect("Dict Dialect");
    }
}
