package com.edwarder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.edwarder.entity.SysDict;
import org.springframework.stereotype.Repository;

/**
 * @Auther: jzhang
 * @Date: 2018/11/2 16:48
 * @Description:
 */
@Repository
public interface SysDictMapper extends BaseMapper<SysDict> {
}
