package com.edwarder.mapper.album;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.edwarder.entity.Album;
import com.edwarder.entity.Film;

/**
 * @Auther: jzhang
 * @Date: 2018/10/26 16:12
 * @Description:
 */
public interface AlbumMapper extends BaseMapper<Album> {
}
