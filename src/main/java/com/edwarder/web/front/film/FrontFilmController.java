package com.edwarder.web.front.film;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.edwarder.entity.Film;
import com.edwarder.global.Global;
import com.edwarder.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Auther: jzhang
 * @Date: 2018/11/2 13:45
 * @Description: 前台影片控制器
 */
@RequestMapping("/f/film")
@Controller
public class FrontFilmController {
    @Autowired
    private FilmService filmService;

    @ModelAttribute("film")
    public Film get(Integer id) {
        if (id != null){
            return filmService.getById(id);
        }else{
            return new Film();
        }
    }

    @RequestMapping("/index")
    public String index(Model model,Integer pageNo) {
        int current = Global.CURRENT_PAGE;
        int pageSize = Global.PAGE_SIZE;
        if(pageNo != null) {
            current = pageNo;
        }
        IPage<Film> page = filmService.findPageList(current,pageSize);
        model.addAttribute("page",page);
        return "front/index";
    }

    @RequestMapping("/details")
    public String details(Model model,Film film) {
        model.addAttribute("film",film);
        return "front/filmDetails";
    }




}
