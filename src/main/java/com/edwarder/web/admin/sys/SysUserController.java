package com.edwarder.web.admin.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.edwarder.activemq.Producer;
import com.edwarder.entity.SysRole;
import com.edwarder.entity.SysUser;
import com.edwarder.service.SysRoleService;
import com.edwarder.service.SysUserService;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.jms.Destination;
import java.util.List;

/**
 * @Auther: jzhang
 * @Date: 2018/10/16 09:19
 * @Description: 系统用户控制器
 */
@Controller
@RequestMapping("/a/user")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private Producer producer;

    @RequestMapping("/login")
    public  String login(){
        return "sys/login";
    }

    @ModelAttribute("sysUser")
    public SysUser get(Integer id) {
        if (id != null){
            return sysUserService.findUserById(id);
        }else{
            return new SysUser();
        }
    }

    /**
     * 获取用户列表
     */
    @RequestMapping("/list")
    public String list(Model model,Integer page,Integer size) {
        IPage<SysUser> pageList = sysUserService.findList(page, size);
        model.addAttribute("list",pageList.getRecords());
        model.addAttribute("count",pageList.getTotal());
        model.addAttribute("curr",pageList.getCurrent());
        return "sys/userList";
    }

    /**
     * 编辑
     */
    @RequestMapping("/form")
    public String form(Model model,SysUser sysUser) {
        //获取所有的角色
        List<SysRole> roleList = sysRoleService.findList();
        model.addAttribute("roleList",roleList);
        model.addAttribute("sysuser",sysUser);
        return "sys/userForm";
    }

    @RequestMapping("/save")
    public String save(SysUser sysUser) {
        sysUserService.saveSysUser(sysUser);
        return "redirect:list";
    }

    /**
     * 修改密码
     */
    @RequestMapping("/modifyPwd")
    public String modifyPwd(SysUser sysUser,Model model) {
        model.addAttribute("sysuser",sysUser);
        return "sys/modifyUserPwd";
    }

    /**
     * 更新密码
     */
    @RequestMapping("/modifyPwdSubmit")
    @ResponseBody
    public String modifyPwdSubmit(SysUser sysUser) {
        Destination destination = new ActiveMQQueue("mytest.queue");
        //推送当前用户的消息
        producer.sendMessage(destination, "密码已修改重新登录！");
        sysUserService.updataPwd(sysUser);
        return "success";
    }
}
