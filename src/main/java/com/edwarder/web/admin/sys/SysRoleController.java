package com.edwarder.web.admin.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.edwarder.entity.SysRole;
import com.edwarder.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @Auther: jzhang
 * @Date: 2018/10/18 16:45
 * @Description: 角色控制器
 */
@Controller
@RequestMapping("/a/role")
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;

    @ModelAttribute("role")
    public SysRole get(Integer id) {
        if (id != null){
            return sysRoleService.findById(id);
        }else{
            return new SysRole();
        }
    }

    @RequestMapping("/list")
    public String list(Model model, Integer page, Integer size) {
        IPage<SysRole> pageList = sysRoleService.findAllList(page,size);
        model.addAttribute("roles",pageList.getRecords());
        model.addAttribute("count",pageList.getTotal());
        model.addAttribute("curr",pageList.getCurrent());
        return "sys/roleList";
    }

    @RequestMapping("/form")
    public String form(Model model, SysRole role) {
        model.addAttribute("sysRole",role);
        return "sys/roleForm";
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(SysRole sysRole,int[] roleMenus) {
        sysRoleService.saveRole(sysRole,roleMenus);
        return "success";
    }

    /**
     * 获取角色授权菜单树
     * @return
     */
    @RequestMapping("/getRoleMenuTree")
    @ResponseBody
    public List<Map<String,Object>> getRoleMenuTree (@RequestParam(required = false) Integer id) {
        List<Map<String,Object>> list = sysRoleService.getRoleMenuTree(id);
        return list;
    }
}
