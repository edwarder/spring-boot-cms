package com.edwarder.web.admin.sys;

import com.edwarder.activemq.Producer;
import com.edwarder.entity.SysMenu;
import com.edwarder.service.SysMenuService;
import com.edwarder.service.SysUserService;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.jms.Destination;
import javax.jms.Message;
import java.util.List;

/**
 * @Auther: jzhang
 * @Date: 2018/10/15 15:51
 * @Description:    后台主页控制台
 */
@Controller
@RequestMapping("/a")
public class SysIndexController {
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private SysUserService sysUserService;
    private String msg;

    @RequestMapping("/")
    public String index(Model model){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        //根据用户查询其对应的菜单
        List<SysMenu> menus = sysMenuService.findListByUserName(userDetails.getUsername());
        model.addAttribute("menus",menus);
        model.addAttribute("sysUser",sysUserService.findUserByName(userDetails.getUsername()));
        return "sys/index";
    }

    @RequestMapping("/console")
    public String console() {
        return "sys/console";
    }

    //监听消息队列中的信息
    @JmsListener(destination = "mytest.queue")
    public void receiveQueue(String text) {
        msg = text;
    }

    @RequestMapping("/getTips")
    @ResponseBody
    public String getTips(Model model) {
        return msg;
    }

}
