package com.edwarder.web.admin.film;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.edwarder.entity.Film;
import com.edwarder.entity.SysDict;
import com.edwarder.service.FilmService;
import com.edwarder.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: jzhang
 * @Date: 2018/10/23 22:33
 * @Description:
 */
@Controller
@RequestMapping("/a/film")
public class FilmController {
    @Autowired
    private FilmService filmService;
    @Autowired
    private SysDictService sysDictService;


    @ModelAttribute("film")
    public Film get(Integer id) {
        if (id != null){
            return filmService.getById(id);
        }else{
            return new Film();
        }
    }

    @RequestMapping("/list")
    public String list() {
        return "film/filmList";
    }

    @RequestMapping("/getlistData")
    @ResponseBody
    public Map<String,Object> getlistData (Model model, Integer pageIndex, Integer pageSize) {
        Map<String,Object> map = new HashMap<>();
        IPage<Film> pageList = filmService.findPageList(pageIndex,pageSize);
        map.put("data",pageList.getRecords());
        map.put("count",pageList.getTotal());
        return map;
    }

    @RequestMapping("/form")
    public String form(Film film,Model model) {
        List<SysDict> typeDicts = sysDictService.list(new QueryWrapper<SysDict>()
                .eq("typecode","filmtype"));
        List<SysDict> lnDicts = sysDictService.list(new QueryWrapper<SysDict>()
                .eq("typecode","language"));
        model.addAttribute("typeDicts",typeDicts);
        model.addAttribute("lnDicts",lnDicts);
        model.addAttribute("film",film);
        return "film/filmForm";
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(Film film) {
        filmService.saveFilm(film);
        return "success";
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(Film film) {
        filmService.removeById(film.getId());
        return "success";
    }

}
