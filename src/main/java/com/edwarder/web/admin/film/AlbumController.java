package com.edwarder.web.admin.film;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.edwarder.config.AppConfig;
import com.edwarder.entity.Album;
import com.edwarder.entity.Film;
import com.edwarder.service.AlbumService;
import com.edwarder.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: jzhang
 * @Date: 2018/10/26 10:41
 * @Description: 剧照控制器
 */
@Controller
@RequestMapping("/a/album")
public class AlbumController {
    @Autowired
    private AppConfig appConfig;

    @Autowired
    private FilmService filmService;

    @Autowired
    private AlbumService albumService;

    /**
     * 相册列表
     */
    @RequestMapping("/list")
    public String list(Film film, Model model) {
        IPage<Film> page = filmService.findPageList(1,10);
        model.addAttribute("filmList",page.getRecords());
        return "film/albumList";
    }

    /**
     * 添加剧照
     */
    @RequestMapping("/form")
    public String add(Film film, Model model) {
        IPage<Album> albums = albumService.page(
                new Page(),
                new QueryWrapper<Album>().eq("filmid",film.getId()));
        model.addAttribute("filmAlbums",albums.getRecords());
        model.addAttribute("film",filmService.getById(film.getId()));
        return "film/albumForm";
    }

    /**
     * 保存相册
     */
    @ResponseBody
    @RequestMapping("/save")
    public String save(@RequestParam("file") MultipartFile[] multipartfiles,
                       @RequestParam("filmId") Integer id) {
        //上传图片
        List<String> filmAlbum = multiUploadFile(multipartfiles);
        //保存影片相册
        filmService.saveAlbum(id,filmAlbum);
        return "success";
    }

    /**
     * 预览相册
     */
    @RequestMapping("albumView")
    public String albumView(Film film, Model model) {
        IPage<Album> albums = albumService.page(
                new Page(),
                new QueryWrapper<Album>().eq("filmid",film.getId()));
        model.addAttribute("filmAlbums",albums.getRecords());
        return "film/albumView";
    }

    //多文件上传
    private List<String> multiUploadFile(MultipartFile[] multipartfiles) {
        List<String> list = new ArrayList<>();
        if(multipartfiles != null && multipartfiles.length>0){
            for(MultipartFile item : multipartfiles)
            {
                String suffix = item.getOriginalFilename().substring(item.getOriginalFilename().lastIndexOf("."));
                String fileName = System.currentTimeMillis()+suffix;
                //获取文件名
                String saveFileName = appConfig.getFilepath()+"/article/"+fileName;
                File dest = new File(saveFileName);
                if(!dest.getParentFile().exists()){ //判断文件父目录是否存在
                    dest.getParentFile().mkdir();
                }
                try {
                    item.transferTo(dest); //保存文件
                    list.add(fileName);//将文件名添加到集合中
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }
}
