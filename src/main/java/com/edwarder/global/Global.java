package com.edwarder.global;

/**
 * @Auther: jzhang
 * @Date: 2018/11/5 14:26
 * @Description: 全局配置类
 */
public class Global {
    /**
     * 默认当前页/每页记录数
     */
    public static final Integer CURRENT_PAGE = 1;
    public static final Integer PAGE_SIZE = 3;
}
