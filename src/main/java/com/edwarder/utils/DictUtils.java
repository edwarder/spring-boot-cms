package com.edwarder.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.edwarder.entity.SysDict;
import com.edwarder.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Auther: jzhang
 * @Date: 2018/11/5 08:42
 * @Description:    字典工具类
 */
@Component
public class DictUtils {
    @Autowired
    private SysDictService service;
    private static SysDictService sysDictService;

    @PostConstruct  //完成对service的注入
    public void init() {
        sysDictService = service;
    }
    /**
     * 获取字段值
     */
    public String getDictValue(String typecode, String name) {
        SysDict sysDict = sysDictService.getOne(new QueryWrapper<SysDict>()
                .eq("typecode",typecode)
                .eq("name",name));
        return sysDict.getValue();
    }
}
